<?php

namespace App\ProfilePicture;
use App\Model\Database as DB;

class ProfilePicture extends DB
{
    Private $name;
    private $picture;
    private $tmp_name;
    private $pic_name;

    public function setData($allFileData, $allPostData)
    {

        if (array_key_exists("name", $allPostData)) {
            $this->name = $allPostData['name'];
        }
        if (array_key_exists("picture", $allFileData)) {
            $this->tmp_name = $allFileData['picture']['tmp_name'];
            $this->pic_name = $allFileData['picture']['name'];
        }
    }
}