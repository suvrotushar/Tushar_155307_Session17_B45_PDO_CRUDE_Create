<?php

namespace App\Hobbies;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Hobbies extends DB
{
    private $name;
    private $hobbie1;
    private $hobbie2;

    public function setData($allPostData){
        if(array_key_exists("name",$allPostData)){
            $this->name = $allPostData['name'];
        }
        if(array_key_exists("hobbie1",$allPostData)){
            $this->hobbie1 = $allPostData['hobbie1'];
        }
        if(array_key_exists("hobbie2",$allPostData)){
            $this->hobbie2 = $allPostData['hobbie2'];
        }
    }
    public function store(){
        $arraData = array($this->name, $this->hobbie1,$this->hobbie2);
        $query = "insert into hobbies (name, hobbie1, hobbie2) VALUES (?,?,?)";
        $sth = $this->DBH->prepare($query);
        $result = $sth->execute($arraData);
        if($result){
            Message::setMessage("Success! Data has been stored Successfully...");
        }else{
            Message::setMessage("Failed! Data has not been stored Successfully...");
        }
        Utility::redirect("create.php");
    }
}