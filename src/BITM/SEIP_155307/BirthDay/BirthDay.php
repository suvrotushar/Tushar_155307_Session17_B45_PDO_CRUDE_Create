<?php
    namespace App\BirthDay;

    use App\Message\Message;
    use App\Model\Database as DB;
    use App\Utility\Utility;

    class BirthDay extends DB
    {
        private $name;
        private $birth_day;

        public function setData($allPostData=null)
        {
            if(array_key_exists("name",$allPostData)){
                $this->name = $allPostData['name'];
            }
            if(array_key_exists("dob",$allPostData)){
                $this->birth_day = $allPostData['dob'];
            }
        }

        public function store(){
            $arraData = array($this->name, $this->birth_day);
            $query = "insert into birth_day(name, birth_day) VALUES (?,?)";
            $STH = $this->DBH->prepare($query);
            $result = $STH->execute($arraData);
            if($result){
                Message::setMessage("Success! Data has been stored Successfully...");
            }else{
                Message::setMessage("Failed! Data has not been stored Successfully...");
            }
            Utility::redirect("create.php");
        }
    }
?>