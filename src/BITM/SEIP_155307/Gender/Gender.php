<?php

namespace App\Gender;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Gender extends DB
{
    private $name;
    private $gender;

    public function setData($allPostData){
        if(array_key_exists("name",$allPostData)){
            $this->name = $allPostData['name'];
        }
        if(array_key_exists("gender",$allPostData)){
            $this->gender = $allPostData['gender'];
        }
    }
    public function store(){
        $arraData = array($this->name, $this->gender);
        $query = "insert into gender (name, gender) VALUES (?,?)";
        $sth = $this->DBH->prepare($query);
        $result = $sth->execute($arraData);
        if($result){
            Message::setMessage("Success! Data has been stored Successfully...");
        }else{
            Message::setMessage("Failed! Data has not been stored Successfully...");
        }
        Utility::redirect("create.php");
    }
}