<?php

namespace App\Email;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Email extends DB
{
    private $name;
    private $email;

    public function setData($allPostData){
        if(array_key_exists("name",$allPostData)){
            $this->name = $allPostData['name'];
        }
        if(array_key_exists("email",$allPostData)){
            $this->email = $allPostData['email'];
        }
    }
    public function store(){
        $arraData = array($this->name, $this->email);
        $query = "insert into email (name, email) VALUES (?,?)";
        $sth = $this->DBH->prepare($query);
        $result = $sth->execute($arraData);
        if($result){
            Message::setMessage("Success! Data has been stored Successfully...");
        }else{
            Message::setMessage("Failed! Data has not been stored Successfully...");
        }
        Utility::redirect("create.php");
    }
}